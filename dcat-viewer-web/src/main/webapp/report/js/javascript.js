jQuery(document).ready(function(){
	$('#header').corner();
	$('.toc').corner();
    $('.details').corner();
    $('.vci').corner();
    $('.ti').corner();
    $('.inuse').corner();
    $('.otherinfo').corner();
    $('.menu span').corner();
    $('.distribution').corner();

    
    $('.term h3 span span.vci-menu').click(function() {
        $(this).parent().parent().nextAll('div').children('.vci').toggle('slow');
        return false;
    });

    $('.term h3 span span.ti-menu').click(function() {
        $(this).parent().parent().nextAll('div').children('.ti').toggle('slow');
        return false;
    });

    $('.term h3 span span.otherinfo-menu').click(function() {
        $(this).parent().parent().nextAll('div').children('.otherinfo').toggle('slow');
        return false;
    });

    $('.term h3 span span.inuse-menu').click(function() {
        $(this).parent().parent().nextAll('div').children('.inuse').toggle('slow');
        return false;
    });
    
    $('.menu span').click(function () {
        $(this).toggleClass('active no-active');
    });
    
    $("#bottom-bar").jixedbar({
        transparent: true,
        opacity: 0.5,
        slideSpeed: "slow",
        roundedCorners: true,
        roundedButtons: true,
        menuFadeSpeed: "slow",
        tooltipFadeSpeed: "fast",
        tooltipFadeOpacity: 0.5
    });
    
    $("#openCloseAll").toggle(function() {
            $('div.term h3 span.menu span').addClass('active');
    	    $('.term div div').show();
            return false;
      }, function() {
            $('div.term h3 span.menu span').removeClass('active');
    	    $(".term div div").hide();
            return false;
      });
    
    $.fn.qtip.styles.parrot = { 
            border: {
               width:1,
               radius: 5
            },
            padding: 10, 
            textAlign: 'center',
    		fontSize: '.9em',
//            width: 300,
            tip: true, // Give it a speech bubble tip with automatic corner detection
            name: 'green' // Style it according to the preset 'cream' style
      }

      $.fn.qtip.defaults.position.corner = {
           tooltip: 'bottomMiddle',
           target: 'topMiddle'
      }

      $.fn.qtip.defaults.hide.when.event = 'mouseout';
      $.fn.qtip.defaults.hide.fixed = true;
    
      $('.icon-info-description').qtip({
          content: 'Add a description using http://purl.org/dc/terms/description',
          style: 'parrot'
      });
      
  	$('#feedback').feedback({
		'position': 'left',
		'mouseoffColor': '#8470ff'
	});
	
    $('#feedback-dialog').dialog({
        autoOpen: false,
		modal: true,
		resizable: false,
		draggable: false
	});
    
    $('#feedback').click(function() {
        $('#feedback-dialog').dialog('open');
        $('#feedback-dialog a').blur(); // remove manually the focus on a elements
		return false;
    });
	
	// active all menus
    $('.menu span.vci-menu').addClass('active');
    $('.menu span.ti-menu').addClass('active');
    $('.menu span.inuse-menu').addClass('active');
    $('.menu span.otherinfo-menu').addClass('active');
	
      
})
