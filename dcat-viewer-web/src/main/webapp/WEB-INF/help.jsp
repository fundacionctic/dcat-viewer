<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> 

<jsp:include page="head.jsp">
	<jsp:param name="title" value="Help page of DCAT viewer, DCAT documentation service" />
</jsp:include>

<body>
<div class="all">
<jsp:include page="header.jsp" />

<h2>Table of contents</h2>
<ul>
<li><a href="#dcat-support">DCAT support at a glance</a></li>
<li><a href="#elements">Supported elements</a></li>
<li><a href="#metadata">Supported metadata</a></li>
<li><a href="#namespace-prefix">Namespace references and prefixes</a></li>
<!-- <li><a href="#rest">Use as a REST service</a></li> -->
<li><a href="#tips">Tips</a>
	<ul>
	<li><a href="#tip-examples">DCAT viewer example compilation</a></li>
	<li><a href="#tip-label">Label order</a></li>
	<li><a href="#tip-description">Description order</a></li>
	<li><a href="#tip-button-rdfa">Button RDFa</a></li>
	<li><a href="#tip-features">Other features</a></li>
	</ul>
</li>
<li><a href="#third-party">License of third-party elements used</a></li>
</ul>
<br/>

<h2 id="dcat-support">DCAT support at a glance</h2>

<img class="centered" src="images/dcat-support.png"></img>

<h2 id="elements">Supported elements</h2>
<table id="metadata-support-all">
  <tbody>
    <tr>
      <th>Element</th>
      <th><code>rdf:type<code></th>
    </tr>
    <tr>
        <td>Catalog</td>
        <td>
            <code>dcat:Catalog</code><br />
    		<code>dcat_deri:Catalog</code><br />
        </td>
    </tr>
    <tr>
        <td>Dataset</td>
        <td>
        	<code>void:Dataset</code><br />
            <code>dcat:Dataset</code><br />
    		<code>dcat_deri:Dataset</code><br />
        </td>
    </tr>
    <tr>
        <td>Distribution</td>
        <td>
            <code>dcat:Distribution</code><br />
    		<code>dcat_deri:Distribution</code><br />
        </td>
    </tr>
  </tbody>
</table>

<h2 id="metadata">Supported metadata</h2>

<p>This table describes the annotated properties relevant for adding metadata.</p>

<table id="metadata-support-all">
  <tbody>
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/rdf-schema/#ch_label"><em>rdfs:label</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The label of the element. The range is a literal with a language tag (optional).</p>
        <p>For example: <code>&lt;rdfs:label xml:lang="en"&gt;Temperature defect&lt;/rdfs:label&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-title"><em>dct:title</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>See <em>rdfs:label</em></p>
        <p>For example: <code>&lt;dct:title xml:lang="en"&gt;Temperature defect&lt;/dct:title&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#title"><em>dc:title</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>See <em>rdfs:label</em></p>
        <p>For example: <code>&lt;dc:title xml:lang="en"&gt;Temperature defect&lt;/dc:title&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/skos-reference/#xl-altLabel"><em>skosxl:altLabel</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>An alternative label of a element. The range of the property is an instance of the class LexicalLabel.</p>
	  	<p>For example: <code>&lt;skosxl:altLabel rdf:resource="http://www.example.org/skos/temperature-fault" /&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/skos-reference/#xl-prefLabel"><em>skosxl:prefLabel</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The preferred label of a element. The range of the property is an instance of the class LexicalLabel.</p>
	  	<p>For example: <code>&lt;skosxl:prefLabel rdf:resource="http://www.example.org/skos/temperature-defect" /&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/skos-reference/#altLabel"><em>skos:altLabel</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>An alternative label of a element. The range is a literal with a language tag.</p>
        <p>For example: <code>&lt;skos:altLabel xml:lang="en"&gt;Temperature fault&lt;/skos:altLabel&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/skos-reference/#prefLabel"><em>skos:prefLabel</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The preferred label of a element. The range is a literal with a language tag (optional). In some cases, it is assumed that <em>prefLabel</em> assumes the role of the <em>label</em> for presentation purposes.</p>
        <p>For example: <code>&lt;skos:prefLabel xml:lang="en"&gt;Temperature defect&lt;/skos:prefLabel&gt;</code></p>
      </td>
    </tr>
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
   </tr>
   <tr>
      <td><a href="http://www.w3.org/TR/rdf-schema/#ch_comment"><em>rdfs:comment</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The description of the element. The range is a literal with a language tag.</p>
		<p>For example: <code>&lt;rdfs:comment xml:lang="en"&gt;It is a defect having to do with temperature issues.&lt;/rdfs:comment&gt;</code></p>      	
      </td>
    </tr>
	<tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-description"><em>dct:description</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>See <em>rdfs:comment</em></p>
		<p>For example: <code>&lt;dct:description xml:lang="en"&gt;It is a defect having to do with temperature issues.&lt;/dct:description&gt;</code></p>      	
      </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#description"><em>dc:description</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>See <em>rdfs:comment</em></p>
      	<p>For example: <code>&lt;dc:description&gt;The Friend of a Friend (FOAF) RDF vocabulary, described using W3C RDFS and OWL.&lt;/dc:description&gt;</code></p>
      </td>
    </tr>
   
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-creator"><em>dct:creator</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
	  <td>
	  	<p>An entity primarily responsible for making the element: a person, an organization or a service. Recommended best practice is to use a FOAF profile to describe a creator.</p>
	  	<p>For example: <code>&lt;dct:creator rdf:resource="http://www.wikier.org/foaf#wikier" /&gt;</code></p>
	  </td>
	</tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#creator"><em>dc:creator</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
	  <td>
	  	<p>An entity primarily responsible for making the element: a person, an organization or a service. Typically, the name of a creator should be used to indicate the entity.</p>
	  	<p>For example: <code>&lt;dc:creator&gt;Sergio Fernández&lt;/dc:creator&gt;</code></p>
	  </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-contributor"><em>dct:contributor</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>An entity responsible for making contributions to the resource: a person, an organization or a service. Recommended best practice is to use a FOAF profile to describe a contributor.</p>
	  	<p>For example: <code>&lt;dct:contributor rdf:resource="http://berrueta.net/foaf.rdf#me" /&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#contributor"><em>dc:contributor</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>An entity responsible for making contributions to the resource: a person, an organization or a service. Typically, the name of a contributor should be used to indicate the entity.</p>
      	<p>For example: <code>&lt;dc:contributor&gt;Diego Berrueta&lt;/dc:contributor&gt;</code></p>
      </td>
    </tr>
	<tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-publisher"><em>dct:publisher</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
	  <td>
	  	<p>An entity responsible for making the element available: a person, an organization or a service. Recommended best practice is to use a FOAF profile to describe a publisher.</p>
	  	<p>For example: <code>&lt;dct:publisher rdf:resource="http://oreilly.com/" /&gt;</code></p>
	  </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#publisher"><em>dc:publisher</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
	  <td>
	  	<p>An entity responsible for making the element available: a person, an organization or a service. Typically, the name of a publisher should be used to indicate the entity.</p>
        <p>For example: <code>&lt;dc:publisher&gt;O'Reilly&lt;/dc:publisher&gt;</code></p>
	  </td>
    </tr>
    <tr>
      <td><a href="http://xmlns.com/foaf/spec/#term_maker"><em>foaf:maker</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>See <em>dct:creator</em>.</p>
	  	<p>For example: <code>&lt;foaf:maker rdf:resource="http://berrueta.net/foaf.rdf#me" /&gt;</code></p>
	  </td>
    </tr>
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-date"><em>dct:date</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The date of creation or publication of the element. Recommended best practice is to use the W3CDTF profile of ISO 8601.</p>
        <p>For example: <code>&lt;dct:date&gt;1981-01-20&lt;/dct:date&gt;</code></p>
      </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dces/#date"><em>dc:date</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The date of creation or publication of the element. Recommended best practice is to use the W3CDTF profile of ISO 8601.</p>
        <p>For example: <code>&lt;dc:date&gt;1981-01-20&lt;/dc:date&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-issued"><em>dct:issued</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>Date of formal issuance (e.g., publication) of the element. Recommended best practice is to use the W3CDTF profile of ISO 8601.</p>
        <p>For example: <code>&lt;dct:issued&gt;1979-03-23&lt;/dct:issued&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-modified"><em>dct:modified</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The date on which the element was changed. Recommended best practice is to use the W3CDTF profile of ISO 8601.</p>
        <p>For example: <code>&lt;dct:modified&gt;2010-07-11&lt;/dct:modified&gt;</code></p>
      </td>
    </tr>    
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-license"><em>dct:license</em></a></td>
	  <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>A legal document describing the copyright license of the element. Recommended best practice is to use Creative Commons licenses and to describe them in RDF with the Creative Commons Rights Expression Language (CC REL).</p>
	  	<p>For example: <code>&lt;dct:license rdf:resource="http://creativecommons.org/licenses/by/3.0/" /&gt;</code></p>
	  </td>
    </tr>    
    <tr>
      <td><a href="http://creativecommons.org/ns"><em>cc:license</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>A legal document describing the copyright license of the element. Recommended best practice is to use Creative Commons licenses and to describe them in RDF with the Creative Commons Rights Expression Language (CC REL).</p>
	  	<p>For example: <code>&lt;cc:license rdf:resource="http://creativecommons.org/licenses/by/3.0/" /&gt;</code></p>
	  </td>
    </tr>
    <tr>
      <td><a href="http://web.resource.org/rss/1.0/modules/cc/"><em>cc-deprecated:license</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>A legal document describing the copyright license of the element. Recommended best practice is to use Creative Commons licenses and to describe them in RDF with the Creative Commons Rights Expression Language (CC REL).</p>
	  	<p>For example: <code>&lt;cc-deprecated:license rdf:resource="http://creativecommons.org/licenses/by/3.0/" /&gt;</code></p>
	  </td>
    </tr>
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/rdf-schema/#ch_seealso"><em>rdfs:seeAlso</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>Specifies a resource that might provide additional information about the element.</p>
	  	<p>For example: <code>&lt;rdfs:seeAlso rdf:resource="http://www.ietf.org/rfc/rfc1766.txt" /&gt;</code></p>
      </td>
    </tr>    
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
    <tr>
      <td><a href="http://xmlns.com/foaf/spec/#term_homepage"><em>foaf:homepage</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The homepage (typically a Web page in HTML format) of the vocabulary.</p>
      	<p>For example: <code>&lt;foaf:homepage rdf:resource="http://rdfs.org/sioc/spec/" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/void#vocabulary"><em>void:vocabulary</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A vocabulary or <em>owl:Ontology</em> whose classes or properties are used in the dataset.</p>
      	<p>For example: <code>&lt;void:vocabulary rdf:resource="http://xmlns.com/foaf/0.1/" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <th rowspan="2">Property</th>
      <th colspan="3">Scope</th>
      <th rowspan="2">Description</th>
    </tr>
    <tr>
      <th>Dataset</th>
      <th>Catalog</th>
      <th>Distribution</th>
    </tr>
	<tr>
      <td><a href="http://rdfs.org/ns/void#dataDump"><em>void:dataDump</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>An RDF dump, partial or complete, of the dataset.</p>
      	<p>For example: <code>&lt;void:dataDump rdf:resource="http://ec.europa.eu/eurostat/ramon/rdfdata/migs.rdf" /&gt;</code></p>
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/void#sparqlEndpoint"><em>void:sparqlEndpoint</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>The SPARQL endpoint of the dataset.</p>
      	<p>For example: <code>&lt;void:sparqlEndpoint rdf:resource="http://risp.asturias.es/sparql" /&gt;</code></p>      	
      </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-identifier"><em>dct:identifier</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A unique identifier of the dataset.</p>
      	<p>For example: <code>&lt;dct:identifier&gt;dataset-1314&lt;/dct:identifier&gt;</code></p>      	
      </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-language"><em>dct:language</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>This refers to the language used in the textual metadata describing titles, descriptions, etc. of the element.</p>
      	<p>For example: <code>&lt;dct:language rdf:resource="http://id.loc.gov/vocabulary/iso639-1/es" /&gt;</code></p>      	
      </td>
    </tr>    
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#elements-language"><em>dc:language</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>The language used in the textual metadata describing titles, descriptions, etc. of the element (Recommended best practice is to use a controlled vocabulary such as RFC 4646).</p>
      	<p>For example: <code>&lt;dc:language&gt;es&lt;/dc:language&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-spatial"><em>dct:spatial</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>The geographical area covered by the element.</p>
      	<p>For example: <code>&lt;dct:spatial rdf:resource="http://datos.gob.es/recurso/sector-publico/territorio/provincia/33" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://xmlns.com/foaf/spec/#term_page"><em>foaf:page</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A page or document about the element.</p>
      	<p>For example: <code>&lt;foaf:page rdf:resource="http://example.org/related-page" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:dataset_keyword"><em>dcat:keyword</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A keyword or tag describing the element.</p>
      	<p>For example: <code>&lt;dcat:keyword&gt;Astronomy&lt;/dcat:keyword&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/dcat"><em>dcat_deri:keyword</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A keyword or tag describing the element.</p>
      	<p>For example: <code>&lt;dcat_deri:keyword&gt;Astronomy&lt;/dcat_deri:keyword&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:dataset_landingpage"><em>dcat:landingPage</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A Web page that can be navigated to in a Web browser to gain access to the element.</p>
      	<p>For example: <code>&lt;dcat:landingPage rdf:resource="http://example.org/dataset3_1.html" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/dcat"><em>dcat_deri:landingPage</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>A Web page that can be navigated to in a Web browser to gain access to the element.</p>
      	<p>For example: <code>&lt;dcat_deri:landingPage rdf:resource="http://example.org/dataset3_1.html" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:dataset_distribution"><em>dcat:distribution</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>Connects a element to its available distributions.</p>
      	<p>For example: <code>&lt;dcat:distribution rdf:resource="http://example.org/distribution4_79" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/dcat"><em>dcat_deri:distribution</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>Connects a element to its available distributions.</p>
      	<p>For example: <code>&lt;dcat_deri:distribution rdf:resource="http://example.org/distribution4_79" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:catalog_dataset"><em>dcat:dataset</em></a></td>
      <td></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>A dataset that is part of the element.</p>
      	<p>For example: <code>&lt;dcat:dataset rdf:resource="http://example.org/dataset3_1" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/dcat"><em>dcat_deri:dataset</em></a></td>
      <td></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>A dataset that is part of the element.</p>
      	<p>For example: <code>&lt;dcat_deri:dataset rdf:resource="http://example.org/dataset3_1" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:distribution_accessurl"><em>dcat:accessURL</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>Could be any kind of URL that gives access to a distribution of the element. E.g. landing page, download, feed URL, SPARQL endpoint.</p>
      	<p>For example: <code>&lt;dcat:accessURL rdf:resource="http://example.org/dataset3_1/sparql" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://vocab.deri.ie/dcat"><em>dcat_deri:accessURL</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>Could be any kind of URL that gives access to a distribution of the element. E.g. landing page, download, feed URL, SPARQL endpoint.</p>
      	<p>For example: <code>&lt;dcat_deri:accessURL rdf:resource="http://example.org/dataset3_1/sparql" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:distribution_downloadurl"><em>dcat:downloadURL</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>This is a direct link to a downloadable file in a given format (e.g. CSV file or RDF file)</p>
      	<p>For example: <code>&lt;dcat:accessURL rdf:resource="http://example.org/dataset3_1.csv" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:_size"><em>dcat:byteSize</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The size of a element in bytes.</p>
      	<p>For example: <code>&lt;dcat:byteSize&gt;5020&lt;/dcat:byteSize&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://dublincore.org/documents/dcmi-terms/#terms-format"><em>dct:format</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The file format of the element.For example:
<pre>&lt;dct:format&gt;
  &lt;dct:IMT&gt;
    &lt;rdf:value&gt;text/csv&lt;/rdf:value&gt;
    &lt;rdfs:label&gt;CSV&lt;/rdfs:label&gt;
  &lt;/dct:IMT&gt;
&lt;dct:format&gt;</pre> 	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:distribution_media_type"><em>dcat:mediaType</em></a></td>
      <td></td><td></td><td><img alt="supported" src="images/tick.png"/></td>
      <td>
      	<p>The media type of the distribution as defined by <a herf="http://www.iana.org/assignments/media-types/">IANA</a>.</p>
      	<p>For example: <code>&lt;dcat:mediaType&gt;application/rdf+xml&lt;/dcat:mediaType&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:catalog_themes"><em>dcat:themeTaxonomy</em></a></td>
      <td></td><td><img alt="supported" src="images/tick.png"/></td><td></td>
      <td>
      	<p>The knowledge organization system (KOS) used to classify catalog's datasets.</p>
      	<p>For example: <code>&lt;dcat:themeTaxonomy rdf:resource="http://example.org/skos/scheme/gemet" /&gt;</code></p>      	
      </td>
    </tr>
    <tr>
      <td><a href="http://www.w3.org/TR/vocab-dcat/#Property:dataset_theme"><em>dcat:theme</em></a></td>
      <td><img alt="supported" src="images/tick.png"/></td><td></td><td></td>
      <td>
      	<p>The main category of the dataset. A dataset can have multiple themes.</p>
      	<p>For example: <code>&lt;dcat:theme rdf:resource="http://example.org/skos/concept/bicycle" /&gt;</code></p>      	
      </td>
    </tr>
    
    </tbody>
</table>

<h2 id="namespace-prefix">Namespace references and prefixes</h2>
<table id="prefix-legend">
  <tbody>
    <tr>
      <th>Prefix</th>
      <th>Namespace</th>
    </tr>
    <tr>
      <td>cc</td>
      <td>http://creativecommons.org/ns#</td>
    </tr>
    <tr>
      <td>cc-deprecated</td>
      <td>http://web.resource.org/cc/</td>
    </tr>
    <tr>
      <td>dc</td>
      <td>http://purl.org/dc/elements/1.1/</td>
    </tr>
    <tr>
      <td>dct</td>
      <td>http://purl.org/dc/terms/</td>
    </tr>
    <tr>
      <td>foaf</td>
      <td>http://xmlns.com/foaf/0.1/</td>
    </tr>
    <tr>
      <td>owl</td>
      <td>http://www.w3.org/2002/07/owl#</td>
    </tr>
    <tr>
      <td>rdfs</td>
      <td>http://www.w3.org/2000/01/rdf-schema#</td>
    </tr>
    <tr>
      <td>skosxl</td>
      <td>http://www.w3.org/2008/05/skos-xl#</td>
    </tr>
    <tr>
      <td>skos</td>
      <td>http://www.w3.org/2004/02/skos/core#</td>
    </tr>
    <tr>
      <td>void</td>
      <td>http://rdfs.org/ns/void#</td>
    </tr>
    <tr>
      <td>dcat</td>
      <td>http://www.w3.org/ns/dcat#</td>
    </tr>
    <tr>
      <td>dcat_deri</td>
      <td>http://vocab.deri.ie/dcat#</td>
    </tr>
  </tbody>
</table>

<!-- 
<h2 id="rest">Use as a REST service</h2>

<h3>Generate report from URL/s</h3> 
<table> 
    <tr><td>URL</td><td colspan="2">/parrot</td></tr> 
    <tr><td>Method</td><td colspan="2">GET</td></tr> 
    <tr><td rowspan="5">Querystring</td><td>documentUri</td><td>URL of an input document</td></tr> 
    <tr><td>mimetype</td><td>Mimetype of the input document. Supported values are
    							<ul>
    								<li><tt>default</tt> Allow content negotiation</li> 
    								<li><tt>application/rdf+xml</tt> OWL ontology</li> 
    								<li><tt>text/n3</tt> N3 ontology</li> 
    								<li><tt>text/turtle</tt> Turtle ontology</li> 
    								<li><tt>application/n-triples</tt> N-triples ontology</li> 
    								<li><tt>application/xhtml+xml</tt> XHTML+RDFa document</li> 
    								<li><tt>text/html</tt> HTML+RDFa documentn</li> 
    								<li><tt>application/rif+xml</tt> RIF XML document</li> 
    								<li><tt>text/x-rif-ps</tt> RIF PS document</li> 
								</ul> 
    </td></tr> 
    <tr><td>profile <span class="optional">(optional)</span></td><td>The profile for generate a customize report. Supported values are
    							<ul>
    								<li><tt>technical</tt></li>
    								<li><tt>business</tt></li>
    							</ul>
    							Default value is <tt>technical</tt>.
    </td></tr> 
    <tr><td>language <span class="optional">(optional)</span></td><td>The language uses to generate the report. The possible values are in the <a href="http://www.iana.org/assignments/language-subtag-registry">IANA registry of language tags</a>. Default value is <tt>en</tt> (English).</td></tr>
    <tr><td>customizeCssUrl  <span class="optional">(optional)</span></td><td>The URL of a customize <abbr title="Cascading Style Sheets"><span class="abbr" title="Cascading Style Sheets">CSS</span></abbr></td></tr> 
    <tr><td rowspan="3">Returns</td><td colspan="2">200 OK</td></tr> 
    <tr><td colspan="2">400 Illegal Request</td></tr>
    <tr><td colspan="2">500 Unexpected error</td></tr> 
</table>

You can send multiple <tt>documentUri</tt> parameter values. In that case, each one should be accompanied by a <tt>mimetype</tt> parameter.


<p>For example:</p>
<p class="code">GET /parrot?documentUri=http://purl.org/dc/terms/&amp;mimetype=default&amp;profile=business&amp;language=en</p>
<br />

<h3>Generate report from direct input</h3> 
<table> 
    <tr><td>URL</td><td colspan="2">/parrot</td></tr> 
    <tr><td>Method</td><td colspan="2">POST</td></tr> 
    <tr><td rowspan="5">Request Body</td><td>documentText</td><td>The input source text.</td></tr> 
    <tr><td>mimetypeText</td><td>Mimetype of the input source text. Supported values are
    							<ul>
    								<li><tt>default</tt> Allow content negotiation</li> 
    								<li><tt>application/rdf+xml</tt> OWL ontology</li> 
    								<li><tt>text/n3</tt> N3 ontology</li> 
    								<li><tt>text/turtle</tt> Turtle ontology</li> 
    								<li><tt>application/n-triples</tt> N-triples ontology</li> 
    								<li><tt>application/xhtml+xml</tt> XHTML+RDFa document</li> 
    								<li><tt>text/html</tt> HTML+RDFa documentn</li> 
    								<li><tt>application/rif+xml</tt> RIF XML document</li> 
    								<li><tt>text/x-rif-ps</tt> RIF PS document</li> 
								</ul> 
    </td></tr> 
    <tr><td>profile <span class="optional">(optional)</span></td><td>The profile for generate a customize report. Supported values are
    							<ul>
    								<li><tt>technical</tt></li>
    								<li><tt>business</tt></li>
    							</ul>
    							Default value is <tt>technical</tt>.
    </td></tr> 
    <tr><td>language <span class="optional">(optional)</span></td><td>The language uses to generate the report. The possible values are in the <a href="http://www.iana.org/assignments/language-subtag-registry">IANA registry of language tags</a>. Default value is <tt>en</tt> (English).</td></tr>
    <tr><td>customizeCssUrl <span class="optional">(optional)</span></td><td>The URL of a customize <abbr title="Cascading Style Sheets"><span class="abbr" title="Cascading Style Sheets">CSS</span></abbr></td></tr> 
    <tr><td rowspan="3">Returns</td><td colspan="2">200 OK</td></tr> 
    <tr><td colspan="2">400 Illegal Request</td></tr>
    <tr><td colspan="2">500 Unexpected error</td></tr> 
</table>

<p>For example:</p>
<p class="code">POST /parrot
	
         documentText=&lt;rdf:RDF xmlns:owl=&quot;http://www.w3.org/2002/07/owl#&quot;
                               xmlns:rdf=&quot;http://www.w3.org/1999/02/22-rdf-syntax-ns#&quot;&gt;
                         &lt;owl:Ontology rdf:about=&quot;http://ontorule-project.eu/resources/steel-30&quot;&gt;
                              &lt;rdfs:label xml:lang=&quot;en&quot;&gt;Steel Ontology&lt;/rdfs:label&gt;
                         &lt;/owl:Ontology&gt;
                      &lt;/rdf:RDF&gt;
         mimetypeText=application/rdf+xml
         profile=business
         language=en
         customizeCssUrl=http://example.org/style.css</p>

You can send multiple <tt>documentText</tt> parameter values. In that case, each one should be accompanied by a <tt>mimetype</tt> parameter.
<br />
<h3>Generate report from existing report</h3> 
<table> 
    <tr><td>URL</td><td colspan="2">/parrot</td></tr> 
    <tr><td>Method</td><td colspan="2">GET</td></tr> 
    <tr><td rowspan="4">Querystring</td><td>reportURL</td><td>URL of the existing report</td></tr> 
    <tr><td>profile <span class="optional">(optional)</span></td><td>The profile for generate a customize report. Supported values are
    							<ul>
    								<li><tt>technical</tt></li>
    								<li><tt>business</tt></li>
    							</ul>
    							Default value is <tt>technical</tt>.
    </td></tr> 
    <tr><td>language <span class="optional">(optional)</span></td><td>The language uses to generate the report. The possible values are in the <a href="http://www.iana.org/assignments/language-subtag-registry">IANA registry of language tags</a>. Default value is <tt>en</tt> (English).</td></tr>
    <tr><td>customizeCssUrl <span class="optional">(optional)</span></td><td>The URL of a customize <abbr title="Cascading Style Sheets"><span class="abbr" title="Cascading Style Sheets">CSS</span></abbr></td></tr> 
    <tr><td rowspan="3">Returns</td><td colspan="2">200 OK</td></tr> 
    <tr><td colspan="2">400 Illegal Request</td></tr>
    <tr><td colspan="2">500 Unexpected error</td></tr> 
</table>

<p>For example:</p>
<p class="code">GET /parrot?reportURL=http://ontorule-project.eu/resources/parrot/examples/previous-report-metadata.html&amp;profile=business&amp;language=en</p>

 -->
<h2 id="tips">Tips</h2>

<h3 id="tip-examples">DCAT viewer example compilation</h3>
<p>In the <a href="examples">DCAT viewer example compilation</a> there are available multiple examples of usage of DCAT.</p>

<h3 id="tip-label">Label order</h3>
<p>If you want to add a <strong>label</strong> to an element, the preferred property order to set it is:</p>
<ol>
  <li>http://www.w3.org/2008/05/skos-xl#prefLabel</li>
  <li>http://www.w3.org/2004/02/skos/core#prefLabel</li>
  <li>http://www.w3.org/2008/05/skos-xl#altLabel</li>
  <li>http://www.w3.org/2004/02/skos/core#altLabel</li>
  <li>http://purl.org/dc/terms/title</li>
  <li>http://purl.org/dc/elements/1.1/title</li>
  <li>http://www.w3.org/2000/01/rdf-schema#label</li>
</ol>

<h3 id="tip-description">Description order</h3>
<p>If you want to add a <strong>description</strong> to an element, the preferred property order to set it is:</p>
<ol>
  <li>http://purl.org/dc/terms/description</li>
  <li>http://purl.org/dc/elements/1.1/description</li>
  <li>http://www.w3.org/2000/01/rdf-schema#comment</li>
</ol>


<h3 id="tip-features">Other features</h3>

<p>If a resource has more than one <em>rdf:type</em>, this resource will be document in the report just once (with no priority to document the resource like a concrete <em>rdf:type</em>).</p>
<p>For instance, if a resource is declared like:</p>
<pre><code>
&lt;rdf:RDF xmlns:rdf=&quot;http://www.w3.org/1999/02/22-rdf-syntax-ns#&quot;&gt;
	&lt;rdf:Description rdf:about=&quot;http://example.org/resource&quot;&gt;
		&lt;rdf:type rdf:resource=&quot;http://www.w3.org/2002/07/owl#Ontology&quot;/&gt;
		&lt;rdf:type rdf:resource=&quot;http://rdfs.org/ns/void#Dataset&quot;/&gt;
	&lt;/rdf:Description&gt;
&lt;/rdf:RDF&gt; 
</code></pre>

<p>The documentation for the resource <em>http://example.org/resource</em> will only appears once (like an ontology or like a dataset).</p>


<h2 id="third-party">License of third-party elements used</h2>
  <p> Some icons has been created by <a href="http://www.famfamfam.com/about/">Mark James</a> and there are distributed under <a href="http://creativecommons.org/licenses/by/2.5/">CreativeCommons-by 2.5</a> license.</p> 

<jsp:include page="footer.jsp" />

</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="report/js/jquery.corner.js"></script>
<script type="text/javascript" src="javascript/help-scripts.js"></script>

<jsp:include page="google-analytics.jsp" />

</body>
</html>
