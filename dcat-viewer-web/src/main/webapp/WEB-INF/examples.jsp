<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> 

<jsp:include page="head.jsp">
	<jsp:param name="title" value="Examples of DCAT viewer" />
</jsp:include>

<body>

<div class="all">

<jsp:include page="header.jsp" />

<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 0, fn:length(req.requestURI)), req.contextPath)}" />

<h2>DCAT viewer pages</h2> 
<ul> 
<li><a href="dcat-viewer">DCAT viewer homepage</a></li> 
<li><a href="help">DCAT viewer help</a></li>
<li><a href="examples">DCAT viewer examples</a></li> 
</ul>

<h2>Datasets</h2> 
<ul> 
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fwww.zaragoza.es%2Fdatosabiertos%2Fsparql%3Fquery%3DCONSTRUCT%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%250D%250AWHERE%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%26format%3Dapplication%252Frdf%252Bxml&mimetype=default&showform=true">Datasets del Ayuntamiento de Zaragoza (RDF/XML)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fwww.zaragoza.es%2Fdatosabiertos%2Fsparql%3Fquery%3DCONSTRUCT%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%250D%250AWHERE%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%26format%3Dtext%252Frdf%252Bn3&mimetype=default&showform=true">Datasets del Ayuntamiento de Zaragoza (RDF N3)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fwww.zaragoza.es%2Fdatosabiertos%2Fsparql%3Fquery%3DCONSTRUCT%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%250D%250AWHERE%2B%7B%250D%250A%2B%2B%253Fcatalogo%2Ba%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523Catalog%253E%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523dataset%253E%2B%253Fdataset.%250D%250A%2B%2B%253Fdataset%2B%253Chttp%253A%252F%252Fvocab.deri.ie%252Fdcat%2523distribution%253E%2B%253Fdistribucion%253B%250D%250A%2B%2B%2B%253Chttp%253A%252F%252Fpurl.org%252Fdc%252Fterms%252FaccrualPeriodicity%253E%2B%253Ffrecuencia%253B%250D%250A%2B%2B%2B%253Fp%2B%253Fo.%250D%250A%2B%2B%253Ffrecuencia%2B%253Ffrec_p%2B%253Ffrec_o%2B.%250D%250A%2B%2B%253Fdistribucion%2B%253Fdist_p%2B%253Fdist_o%2B.%250D%250A%7D%26format%3Dapplication%2Fx-turtle&mimetype=default&showform=true">Datasets del Ayuntamiento de Zaragoza (RDF TURTLE)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fdatos.gijon.es%2Fset.rdf&mimetype=default&showform=true">Datasets del Ayuntamiento de Gijón (RDF XML)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fwww.gencat.cat%2Fopendata%2Frecursos%2Fdatasets%2Fcataleg.rdf&mimetype=default&showform=true">Datasets del Ayuntamiento de la Generalitat de Catalunya (RDF XML)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=${baseURL}/examples/dcat.rdf&mimetype=default&showform=true">Synthetic DCAT example (show form)</a></li> 
<li><a href="dcat-viewer?documentUri=${baseURL}/examples/gijon-dcat-extract.rdf&mimetype=default&showform=true">Extract of Gijón DCAT catalog (show form)</a></li> 
</ul> 
<h2>VoID Datasets</h2> 
<ul> 
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fec.europa.eu%2Feurostat%2Framon%2Frdfdata%2Fvoid.rdf&mimetype=default&profile=business&language=en&showform=true">Eurostat's Metadata Server RAMON [external resource] (show form)</a></li> 
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fsouthampton.rkbexplorer.com%2F.well-known%2Fvoid&mimetype=default&showform=true">Linked Data Repository (School of Electronics and Computer Science at the University of Southampton)  [external resource] (show form)</a></li>
<li><a href="dcat-viewer?documentUri=http%3A%2F%2Fsouthampton.rkbexplorer.com%2Fmodels%2Fvoid.ttl&mimetype=default&showform=true">List of datasets from School of Electronics and Computer Science at the University of Southampton  [external resource] (show form)</a></li>
</ul> 


<h2>Internal resources</h2> 
<ul> 
<li><a href="examples/dcat.rdf">Synthetic DCAT example file (RDF/XML)</a></li>
<li><a href="examples/gijon-dcat-extract.rdf">Extract of Gijón DCAT catalog</a></li>

</ul> 

<jsp:include page="footer.jsp" />

</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="report/js/jquery.corner.js"></script>
<script type="text/javascript" src="javascript/help-scripts.js"></script>

<jsp:include page="google-analytics.jsp" />

</body>
</html>
