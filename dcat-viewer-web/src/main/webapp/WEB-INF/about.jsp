<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> 

<jsp:include page="head.jsp">
	<jsp:param name="title" value="About page of DCAT viewer, DCAT documentation service" />
</jsp:include>


<body>

<div class="all">

<jsp:include page="header.jsp" />

<h2 id="what-is">What is DCAT viewer</h2>
<p>DCAT viewer is a viewer of <a href="http://www.w3.org/TR/2014/REC-vocab-dcat-20140116/">DCAT (Data Catalog Vocabulary)</a>, an RDF vocabulary designed to facilitate interoperability between data catalogs published on the Web. This document defines the schema and provides examples for its use.</p>
<p>DCAT viewer is a fork of <a href="https://bitbucket.org/fundacionctic/parrot">PARROT (an OWL and RIF documentation tool)</a> developed by <a href="http://www.fundacionctic.org/">Fundación CTIC</a>.</p>
<p>DCAT viewer software modules are licensed under <a href="http://www.eclipse.org/legal/epl-v10.html">Eclipse Public License - v 1.0</a>, a business-friendly OSS license.</p>


<h3>Team</h3>
<ul>
<li>Carlos Tejo Alonso (<code>carlos.tejo@fundacionctic.org</code>)</li>
<li>Martín Álvarez Espinar (<code>martin.alvarez@fundacionctic.org</code>)</li>
<li>Chus García (<code>chus.garcia@fundacionctic.org</code>)</li>
</ul>

<jsp:include page="footer.jsp" />

</div>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="report/js/jquery.corner.js"></script>
<script type="text/javascript" src="javascript/help-scripts.js"></script>

<jsp:include page="google-analytics.jsp" />

</body>
</html>
