	<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="custom-functions.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> 

<jsp:include page="head.jsp">
	<jsp:param name="title" value="DCAT viewer documentation service" />
</jsp:include>

<body>
	<div class="all"> 

	<jsp:include page="header.jsp" />


		<c:if test="${ ! empty errorsGeneral}" >    	
    	<div class="inputData" id="inputDataReport">
			<h2 class="error">Errors</h2>
			<div class="inputDataReport">
		    	<ul class="error">
					<c:forEach var="line" items="${errorsGeneral}">
		         		<li class="error"><span><c:out value="${line}"/></span></li>
		      		</c:forEach>
				</ul>
			</div>
		</div>
		</c:if>

		<div class="inputData"> 
		<div id="tabs"> 
		    <ul> 
		        <li><a href="#tabs-1">by URL</a></li> 
		        <li><a href="#tabs-2">by direct input</a></li> 
		        <li><a href="#tabs-3">by file upload</a></li>
		    </ul>            
		        <div id="tabs-1"> 
		        	<h2>by URL</h2> 
					<form method="get" action="">
		        	<c:choose>
    					<c:when test='${not empty paramValues.documentUri}'>
				        	<%-- For every String[] item of paramValues... --%>
		      				<c:forEach var='uri' items='${paramValues.documentUri}' varStatus='uriStatus'> 
				        	<p class="uri-input<c:if test="${fn:contains(errorsUri, uri)}"> error</c:if>">
								<label <c:if test='${uriStatus.first}'>for="documentUri"</c:if> title="URL of the page containing the OWL/RIF document" class="uri">URL: </label>
				        		<input <c:if test='${uriStatus.first}'>id="documentUri"</c:if> name="documentUri" value="<c:out value="${uri}" escapeXml="true"/>" type="text" size="100" />
							<select name="mimetype">
								<option value="default" <c:if test="${paramValues.mimetype[uriStatus.index] eq 'default'}">selected="selected"</c:if>>Allow content negotiation</option>
								<jsp:include page="mimetypes.jsp">
									<jsp:param name="mimetypeSimple" value="false"/>
									<jsp:param name="mimetypeObtained" value="${paramValues.mimetype[uriStatus.index]}" />
								</jsp:include>
							</select>							 
							<span class="removeURI">remove</span>
							<c:if test='${uriStatus.first}'>
								<%-- Only for the first input --%>
								<span class="uriHint">(for example: <tt id="example">http://idi.fundacionctic.org/dcat-viewer/examples/gijon-dcat-extract.rdf</tt>)</span>
							</c:if>
							<c:if test="${fn:contains(errorsUri,uri)}">
								<span class="error">${fn:getMessage(errorsUri, uri)}</span>
							</c:if>
							</p>
		 			        </c:forEach>
					    </c:when>
					    <c:otherwise>
				        	<p class="uri-input"><label title="URL of the page containing the OWL/RIF document"
							for="documentUri" class="uri">URL: </label><input id="documentUri" name="documentUri" value="http://" type="text" size="100" />
							<select name="mimetype">
								<option value="default" selected="selected">Allow content negotiation</option>
								<jsp:include page="mimetypes.jsp">
									<jsp:param name="mimetypeSimple" value="true"/>  
								</jsp:include> 
							</select>
							<span class="removeURI">remove</span>
							<span class="uriHint">(for example: <tt id="example">http://idi.fundacionctic.org/dcat-viewer/examples/gijon-dcat-extract.rdf</tt>)</span>
							</p>
						</c:otherwise>
					</c:choose>

					<p id="addURI"><span id="addURILink">add another URL</span></p>
					<div class="buttons">
						<button type="submit" class="generate">Generate documentation</button>
					</div>
					
					</form>
		       	</div>
			
		        <div id="tabs-2"> 
		        	<h2>by direct input</h2> 
					<form method="post" action="?">
					
		        	<c:choose>
    					
    					<c:when test='${not empty paramValues.documentText}'>
				        	<%-- For every String[] item of paramValues... --%>
		      				<c:forEach var='text' items='${paramValues.documentText}' varStatus='textStatus'>
		      					<div class="direct-input"><label title="Text of the document" class="text">Enter your document:</label><br />
				        		<textarea name="documentText" cols="150" rows="15" style="width:80%"><c:out value="${text}" escapeXml="true"/></textarea>
		      				
								<p><span class="direct-input">This is an : 
								<select name="mimetypeText">
									<jsp:include page="mimetypes.jsp">
										<jsp:param name="mimetypeSimple" value="false"/>
										<jsp:param name="mimetypeObtained" value="${paramValues.mimetypeText[textStatus.index]}" />
									</jsp:include>
								</select>
						    	</span>							 
						    	<span class="removeText">remove</span>
							</p>
							</div>
		 			        </c:forEach>
					    </c:when>
					    
					    <c:otherwise>
				        	<div class="direct-input"><label title="Text of the document" class="text">Enter your document:</label><br />
				        	<textarea name="documentText" cols="150" rows="15" style="width:80%"><c:if test='${not empty param.documentText}'><c:out value="${param.documentText}" escapeXml="true"/></c:if></textarea>
				        	
							<p><span class="direct-input">This is an : 
							  <select name="mimetypeText">
								<jsp:include page="mimetypes.jsp">
									<jsp:param name="mimetypeSimple" value="true"/>
								</jsp:include> 
						      </select>
						    </span>
						    <span class="removeText">remove</span></p>
						    </div>
						</c:otherwise>
						
					</c:choose>

				    <p id="addText"><span id="addTextLink">add another text</span></p>
					<div class="buttons">
						<button type="submit" class="generate">Generate documentation</button>
					</div>

					</form>
		        </div> 

		        <div id="tabs-3"> 
		        	<h2>by file upload</h2> 
					<form method="post" action="?" enctype="multipart/form-data">
						<p>
						<label title="File to be documented">File: </label><input type="file" name="datafile" size="40"/>
						This file is a: 
						<select name="mimetypeFile">
							<option value="default" selected="selected">Autodetect (if possible)</option>
							<jsp:include page="mimetypes.jsp">
								<jsp:param name="mimetypeSimple" value="true"/>  
							</jsp:include> 
						</select>
					    </p>
					    <p><button id="addFile">add another file</button></p>
						<div class="buttons">
							<button type="submit" class="generate">Generate documentation</button>
						</div>
					    
					</form>
				</div> <!--  /tab3 -->
		</div> 
		    <div id="tipOfTheDay">
		    <span >Did you know ...</span>
		    </div>
		    <div id="video"><span ><a href="http://www.youtube.com/watch?v=uN2XQA6TVRc">Watch a video demo of DCAT viewer on YouTube</a></span></div>
		    <p><img src="images/help.png" alt="help" class="help" />Do you need <a href="help">help</a>?</p>
	</div>    
    
    <jsp:include page="footer.jsp" />
    
    </div> 

	<!-- Generating -->
	<div id="generating-dialog" title="Thank you for using DCAT viewer">
		<p><img src="images/loading-spinner.gif" alt=""></img></p>
		<p>DCAT viewer is generating the report</p> 
	</div> 
	
<!-- standalone -->
<!-- <script type="text/javascript" src="standalone/ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="standalone/ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>

<script type="text/javascript" src="javascript/jquery.randomContent.js"></script>
<script type="text/javascript" src="report/js/jquery.corner.js"></script>
<script type="text/javascript" src="javascript/scripts.js"></script>

<c:if test='${not empty paramValues.documentText}'>
<script type="text/javascript">//<![CDATA[ 
jQuery(document).ready(function(){
	$("#tabs").tabs("select", 1);
});
//]]></script>
</c:if>

<jsp:include page="google-analytics.jsp" />

</body> 

</html> 