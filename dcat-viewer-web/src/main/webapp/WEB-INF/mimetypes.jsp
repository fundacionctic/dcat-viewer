<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${param.mimetypeSimple eq 'true'}">
	<option value="application/rdf+xml">RDF/XML document</option>
	<option value="text/n3">N3 document</option>
	<option value="text/turtle">Turtle document</option>
	<option value="application/n-triples">N-triples document</option>								
	<option value="application/xhtml+xml">XHTML+RDFa document</option>
	<option value="text/html">HTML+RDFa document</option>
</c:if>

<c:if test="${param.mimetypeSimple eq 'false'}">
	<option value="application/rdf+xml"   <c:if test="${param.mimetypeObtained eq 'application/rdf+xml'}">selected="selected"</c:if>>RDF/XML document</option>
	<option value="text/n3"				  <c:if test="${param.mimetypeObtained eq 'text/n3'}">selected="selected"</c:if>>N3 document</option>
	<option value="text/turtle" 		  <c:if test="${param.mimetypeObtained eq 'text/turtle'}">selected="selected"</c:if>>Turtle document</option>
	<option value="application/n-triples" <c:if test="${param.mimetypeObtained eq 'application/n-triples'}">selected="selected"</c:if>>N-triples document</option>
	<option value="application/xhtml+xml" <c:if test="${param.mimetypeObtained eq 'application/xhtml+xml'}">selected="selected"</c:if>>XHTML+RDFa document</option>
	<option value="text/html" 			  <c:if test="${param.mimetypeObtained eq 'text/html'}">selected="selected"</c:if>>HTML+RDFa document</option>
</c:if>
