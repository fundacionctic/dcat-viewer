<%@ page pageEncoding="UTF-8"%>
<div id="footer"> 
  <p id="logo"> 
    <a href="http://www.fundacionctic.org/"><img src="images/ctic.png" alt="Fundacion CTIC" /></a> 
  </p> 
  <p> 
    <a href="https://bitbucket.org/fundacionctic/dcat-viewer">DCAT viewer</a> is a DCAT documentation service developed <a href="http://www.fundacionctic.org/">Fundaci&oacute;n CTIC</a>.
  </p> 
  <p> 
    <a href="./dcat-viewer">Home</a> | <a href="./examples">Examples</a> | <a href="./about">About</a> | <a href="./help">Help</a> | <a href="./contact">Contact</a>
  </p> 
</div>
