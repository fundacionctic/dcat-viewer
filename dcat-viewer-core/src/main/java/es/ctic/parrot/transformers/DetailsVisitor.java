package es.ctic.parrot.transformers;

import java.util.Locale;

import org.apache.log4j.Logger;

import es.ctic.parrot.de.Catalog;
import es.ctic.parrot.de.Dataset;
import es.ctic.parrot.de.Distribution;
import es.ctic.parrot.de.OntologyIndividual;
import es.ctic.parrot.docmodel.CatalogDetailView;
import es.ctic.parrot.docmodel.DatasetDetailView;
import es.ctic.parrot.docmodel.DistributionDetailView;
import es.ctic.parrot.docmodel.Document;
import es.ctic.parrot.docmodel.OntologyIndividualDetailView;

/**
 * 
 * Visitor that fills a <code>document</code> (this document will be used later for presentation issues).
 * <code>DetailsVisitor</code> is an implementation of the Visitor pattern.
 * Please refer to the Gang of Four book of Design Patterns for more details on the Visitor pattern.
 * 
 * @author <a href="http://www.fundacionctic.org">CTIC Foundation</a>
 * @version 1.1
 * @since 1.0
 *
 */
public class DetailsVisitor extends AbstractDocumentableObjectVisitor {
    
    private static final Logger logger = Logger.getLogger(DetailsVisitor.class);
    
    private Document document;
    private Locale locale;
    
    /**
     * Constructs a details visitor.
     * @param document the document to be fulfilled.
     * @param locale the locale.
     */
    public DetailsVisitor(Document document, Locale locale) {
        this.document = document;
        this.locale = locale;
    }
    
    /**
     * Visits the <code>individual</code>.
     * @param object the individual.
     * @return the details view generated.
     *  
     */
	@Override
	public Object visit(OntologyIndividual object) throws TransformerException {
	    logger.debug("Visiting individual " + object);
		OntologyIndividualDetailView details = OntologyIndividualDetailView.createFromIndividual(object, locale);		
		document.addOntologyIndividualDetailView(details);
		return details;
	}
	

    /**
     * Visits the <code>dataset</code>.
     * @param object the dataset.
     * @return the details view generated.
     *  
     */
	@Override
	public Object visit(Dataset object) throws TransformerException {
	    logger.debug("Visiting dataset " + object);
	    DatasetDetailView details = DatasetDetailView.createFromDataset(object, locale);
	    for(Distribution distribution : object.getDistributions()){
	    	details.addDistributionDetailView((DistributionDetailView)distribution.accept(this));
	    }
		document.addDatasetDetailView(details);
		return details;
	}
	
    /**
     * Visits the <code>catalog</code>.
     * @param object the catalog.
     * @return the details view generated.
     *  
     */
	@Override
	public Object visit(Catalog object) throws TransformerException {
	    logger.debug("Visiting catalog " + object);
	    CatalogDetailView details = CatalogDetailView.createFromCatalog(object, locale);
		document.addCatalogDetailView(details);
		return details;
	}
	
    /**
     * Visits the <code>distribution</code>.
     * @param object the distribution.
     * @return the details view generated.
     *  
     */
	@Override
	public Object visit(Distribution object) throws TransformerException {
		logger.debug("Visiting distribution " + object);
		DistributionDetailView details = DistributionDetailView.createFromDistribution(object, locale);
		return details;
	}
}
