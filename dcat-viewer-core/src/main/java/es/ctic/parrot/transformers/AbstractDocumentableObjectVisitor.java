package es.ctic.parrot.transformers;

import es.ctic.parrot.de.Catalog;
import es.ctic.parrot.de.Dataset;
import es.ctic.parrot.de.Distribution;
import es.ctic.parrot.de.DocumentableObject;
import es.ctic.parrot.de.DocumentableObjectRegister;
import es.ctic.parrot.de.OntologyIndividual;

/**
 * 
 * A default implementation of <code>DocumentableObjectVisitor</code>.
 * 
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 *
 */
public class AbstractDocumentableObjectVisitor implements
        DocumentableObjectVisitor {


    /**
     * Visits the documentable elements that are registered.
     * @param register the <code>register</code> to visit.
     * @throws TransformerException if a failed transformation operation occurs.
     * @return always null.
     */
    public Object visit(DocumentableObjectRegister register) throws TransformerException{
        // default implementation: visit all the objects in the register
        for ( DocumentableObject documentableObject : register.getAllRegisteredDocumentableObjects() ) {
            documentableObject.accept(this);
        }
        return null;
    }


	/**
     * Does nothing.
     * @param object the <code>individual</code> to visit.
     * @throws TransformerException if a failed transformation operation occurs.
     * @return always null.
     */
	public Object visit(OntologyIndividual object) throws TransformerException{
        // default implementation: do nothing
		return null;
	}

    /**
     * Does nothing.
     * @param object the <code>data set</code> to visit.
     * @throws TransformerException if a failed transformation operation occurs.
     * @return always null.
     */
    public Object visit(Dataset object) throws TransformerException{
        // default implementation: do nothing
		return null;        
    }
    
    /**
     * Does nothing.
     * @param object the <code>catalog</code> to visit.
     * @throws TransformerException if a failed transformation operation occurs.
     * @return always null.
     */
    public Object visit(Catalog object) throws TransformerException{
        // default implementation: do nothing
		return null;        
    }

    /**
     * Does nothing.
     * @param object the <code>distribution</code> to visit.
     * @throws TransformerException if a failed transformation operation occurs.
     * @return always null.
     */
	public Object visit(Distribution object) throws TransformerException {
		// TODO Auto-generated method stub
		return null;
	}
}
