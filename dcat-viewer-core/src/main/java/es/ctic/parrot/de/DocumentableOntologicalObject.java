package es.ctic.parrot.de;


/**
 * An ontological element to be documented by Parrot. This interface encapsulates
 * different ontological documentable elements.
 * 
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 * 
 */
public interface DocumentableOntologicalObject extends DocumentableObject,Comparable<DocumentableOntologicalObject>, Versionable{

	
	/**
     * Returns <code>true</code> if, and only if, this ontological element is deprecated.
     * @return <code>true</code> if this ontological element is deprecated, otherwise <code>false</code>.
     */
	public abstract boolean isDeprecated();
	
	/**
	 * Returns the status.
	 * @return the status.
	 */
	public abstract String getStatus();
	
}
