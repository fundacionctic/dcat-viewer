package es.ctic.parrot.de;

/**
 * A SKOS Concept.
 * 
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 *
 */
public class SKOSConcept {
	
	private String uri;
	private String label;
	private String inScheme;


	public SKOSConcept(String uri, String label, String theme) {
		super();
		
		this.setUri(uri);
		
		if (label == null){
			this.setLabel(uri);
		} else {
			this.setLabel(label);
		}
		
		if (theme != null){
			this.setInScheme(theme);
		}
	}

	
	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the inScheme
	 */
	public String getInScheme() {
		return inScheme;
	}

	/**
	 * @param inScheme the inScheme to set
	 */
	public void setInScheme(String inScheme) {
		this.inScheme = inScheme;
	}

}