package es.ctic.parrot.docmodel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;

import es.ctic.parrot.reader.Input;

/**
 * 
 * A document is a group of detailed views that will be used by a generator.
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 *
 */
public class Document {

	private Locale locale;
	private String title;
	private final Collection<Input> inputs = new HashSet<Input>();
    private final Set<OntologyIndividualDetailView> ontologyIndividualDetailViews = new HashSet<OntologyIndividualDetailView>();
    private final Set<DatasetDetailView> datasetDetailViews = new HashSet<DatasetDetailView>();
    private final Set<CatalogDetailView> catalogDetailViews = new HashSet<CatalogDetailView>();

    private Collection<String> languages;
	private String reportURL;
    
	private static final org.apache.log4j.Logger logger = Logger.getLogger(Document.class);


    /**
     * Constructs a document using the given locale.
     * @param locale the locale.
     */
    public Document(Locale locale) {
    	this.setLocale(locale);
    }
	
    /**
     * Returns the title of this document.
     * @return the title of this document.
     */
	public String getTitle() {
		if (this.title == null) {
			this.title = generateTitle();
		}
		
		return this.title;
	}

	/**
	 * Sets the report URL.
	 * @param reportURL the report URL to set.
	 */
	public void setReportURL(String reportURL) {
		this.reportURL = reportURL;
	}

	/**
 	 * Returns the report URL.
	 * @return the report URL.
	 */
	public String getReportURL() {
		return reportURL;
	}


	/**
	 * Returns the sorted collection of individual views for this document. Elements are sorted alphabetically by label.
	 * @return the sorted collection of individual views for this document.
	 */
    public Collection<OntologyIndividualDetailView> getOntologyIndividualDetailViews() {
		List<OntologyIndividualDetailView> listOntologyIndividualDetailViews = new LinkedList<OntologyIndividualDetailView>(this.ontologyIndividualDetailViews);
		Collections.sort(listOntologyIndividualDetailViews, new DetailViewComparator());
		return listOntologyIndividualDetailViews;
    }


	/**
	 * Returns the sorted collection of dataset views for this document. Elements are sorted alphabetically by label.
	 * @return the sorted collection of dataset views for this document.
	 */
	public Collection<DatasetDetailView> getDatasetDetailViews() {
		List<DatasetDetailView> listDatasetDetailsViews = new LinkedList<DatasetDetailView>(this.datasetDetailViews);
		Collections.sort(listDatasetDetailsViews, new DetailViewComparator());
		return listDatasetDetailsViews;
	}

	/**
	 * Returns the sorted collection of catalog views for this document. Elements are sorted alphabetically by label.
	 * @return the sorted collection of catalog views for this document.
	 */
	public Collection<CatalogDetailView> getCatalogDetailViews() {
		List<CatalogDetailView> listCatalogDetailsViews = new LinkedList<CatalogDetailView>(this.catalogDetailViews);
		Collections.sort(listCatalogDetailsViews, new DetailViewComparator());
		return listCatalogDetailsViews;
	}
	
     
    /** 
     * Adds a given detailed individual view into this document.
     * @param details the detailed individual view.
     */
	public void addOntologyIndividualDetailView(OntologyIndividualDetailView details) {
		this.ontologyIndividualDetailViews.add(details);
	}
	
    /** 
     * Adds a given detailed dataset view into this document.
     * @param details the detailed dataset view.
     */
    public void addDatasetDetailView(DatasetDetailView details) {
        this.datasetDetailViews.add(details);
    }
    
    /** 
     * Adds a given detailed catalog view into this document.
     * @param details the detailed catalog view.
     */
    public void addCatalogDetailView(CatalogDetailView details) {
        this.catalogDetailViews.add(details);
    }
    

	/**
	 * Returns a collection of inputs.
	 * @return a collection of inputs.
	 */
	public Collection<Input> getInputs() {
		return inputs;
	}
	
	/**
	 * Sets a collection of inputs.
	 * @param inputs a collection of inputs to set.
	 */
	public void setInputs(Collection<Input> inputs){
		this.inputs.addAll(inputs);
	}


	/**
	 * Set the languages.
	 * @param languages the languages to set.
	 */
	public void setLanguages(Collection<String> languages) {
		this.languages = languages;
	}

	/**
	 * Returns the languages.
	 * @return the languages.
	 */
	public Collection<String> getLanguages() {
		return languages;
	}
	
	/**
	 * Returns <code>true</code> if all the inputs are persistent, otherwise <code>false</code>. For example, a <code>URL</code> or a <code>file</code> is a persistent input.
     * @return <code>true</code> if all the inputs are persistent, otherwise <code>false</code>. For example, a <code>URL</code> or a <code>file</code> is a persistent input.
	 */
	public boolean isReloadable(){
		for(Input input: getInputs()){
			if (input.isPersistent() == false){
				return false;
			}
		}
		return true;
	}

	/**
	 * Sets the locale.
	 * @param locale the locale to set.
	 */
	private void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	/**
	 * Returns the locale.
	 * @return the locale.
	 */
	public Locale getLocale() {
		return locale;
	}
	
	/**
	 * Returns the main language of the document.
	 * @return the main language of the document
	 */
	public String getMainLanguage() {
		return locale.getLanguage();
	}

	
	/**
	 * Return a generated title for the document
	 * @return the generated title.
	 */
	private String generateTitle() {
//		logger.debug("dataset="+datasetDetailViews.size());
		
		if ( catalogDetailViews.size() == 1){
			return catalogDetailViews.iterator().next().getLabel();
		} else if ( datasetDetailViews.size() == 1){
			return datasetDetailViews.iterator().next().getLabel();
		} else {
			return "Report for several artifacts";
		}
	}

}
/**
 * Compares alphabetically by label of each <code>DetailView</code> 
 *
 */
class DetailViewComparator implements Comparator<DetailView> {

	// This comparator is not consistent with equals()
	public int compare(DetailView arg0, DetailView arg1) {
		if (arg0.getLabel() != null && arg1.getLabel() != null) {
			return  arg0.getLabel().toLowerCase().compareTo(arg1.getLabel().toLowerCase());
		} else {
			return 0; // FIXME change comparable method
		}
	}
	
}

