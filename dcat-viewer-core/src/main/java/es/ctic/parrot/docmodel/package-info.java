/**
 * This package manages the heterogeneity of detailed elements to be presented.
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 */

package es.ctic.parrot.docmodel;
