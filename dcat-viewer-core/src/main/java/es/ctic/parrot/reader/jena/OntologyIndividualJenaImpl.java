package es.ctic.parrot.reader.jena;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.shared.JenaException;

import es.ctic.parrot.de.DocumentableObjectRegister;
import es.ctic.parrot.de.OntologyIndividual;
import es.ctic.parrot.de.Triple;
import es.ctic.parrot.transformers.DocumentableObjectVisitor;
import es.ctic.parrot.transformers.TransformerException;

/**
 * An implementation of {@link es.ctic.parrot.de.OntologyIndividual} coupled to <a href="http://openjena.org/">Jena</a>.
 * 
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 *
 */
public class OntologyIndividualJenaImpl extends AbstractJenaDocumentableObject
		implements OntologyIndividual {

    private static final Logger logger = Logger.getLogger(OntologyIndividualJenaImpl.class);

	/**
	 * Constructs an ontology individual.
	 * @param individual the Jena individual to set.
	 * @param register the register to set
	 * @param annotationStrategy the annotation strategy to set.
	 */
	public OntologyIndividualJenaImpl(Individual individual, DocumentableObjectRegister register, OntResourceAnnotationStrategy annotationStrategy) {
		super(individual, register, annotationStrategy);
	}
	
	private Individual getIndividual(){
		return getOntResource().asIndividual();
	}

	public Object accept(DocumentableObjectVisitor visitor) throws TransformerException {
        try {
        	return visitor.visit(this);
        } catch (JenaException e) {
        	throw new TransformerException(e);
        }
	}
	
    public String getKindString() {
        return Kind.ONTOLOGY_INDIVIDUAL.toString();
    }
    
	public Collection<Triple> getTriplesAsSubject() {
   		return getAnnotationStrategy().getTriplesAsSubject(getOntResource());
	}

	public Collection<Triple> getTriplesAsObject() {
   		return getAnnotationStrategy().getTriplesAsObject(getOntResource());
	}

}
