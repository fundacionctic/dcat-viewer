/**
 * 
 * This package holds classes for an OWL, RDF and RDFa reader that uses Jena.
 *  
 * @author Carlos Tejo Alonso (<a href="http://www.fundacionctic.org">Fundación CTIC</a>)
 * @version 1.1
 * @since 1.0
 */

package es.ctic.parrot.reader.jena;
