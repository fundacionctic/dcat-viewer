package es.ctic.parrot.utils;

public class URIs {

    final public static class DCT {
        private static final String PREFIX = "http://purl.org/dc/terms/";
        public static final String IMT_class = PREFIX+"IMT";  
        public static final String Frecuency_class = PREFIX+"Frecuency";
        public static final String PeriodOfTime_class = PREFIX+"PeriodOfTime";
        public static final String RightsStatement = PREFIX+"RightsStatement";
        
        
        
    }

    final public static class FOAF {
        private static final String PREFIX = "http://xmlns.com/foaf/0.1/";
        public static final String Organization_class = PREFIX+"Organization";    	
        public static final String Person_class = PREFIX+"Person";    	
    }

}
