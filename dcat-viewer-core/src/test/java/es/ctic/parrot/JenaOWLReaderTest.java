package es.ctic.parrot;

import junit.framework.TestCase;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import es.ctic.parrot.reader.jena.JenaOWLReader;
import es.ctic.parrot.utils.URIs.DCT;

public class JenaOWLReaderTest extends TestCase {

	public void testIsDCATAllowIndividual() {
		JenaOWLReader reader = new JenaOWLReader();
		OntModel model = ModelFactory.createOntologyModel();
		final String individual_URI = "http://example.org/individual";
		Individual individual = model.createIndividual(individual_URI, model.createResource(DCT.IMT_class));
	
		boolean flag = reader.isDCATAllowIndividual(individual);
		
		assert (flag == false); // It is not working
		
		if (flag == true){
			fail("The individual is not allowed and the method returns true");
		}
	}
}
