# DCAT viewer #
DCAT viewer is a viewer of [DCAT (Data Catalog Vocabulary)](http://www.w3.org/TR/2014/REC-vocab-dcat-20140116/), an RDF vocabulary designed to facilitate interoperability between data catalogs published on the Web. This document defines the schema and provides examples for its use.


----
## Fork of PARROT
DCAT viewer is a fork of [PARROT (an OWL and RIF documentation tool)](https://bitbucket.org/fundacionctic/parrot) developed by [Fundación CTIC](http://www.fundacionctic.org/).


----
## License
This project is licensed under the [Eclipse Public License - v 1.0](http://www.eclipse.org/legal/epl-v10.html). Read each project LICENSE file for further information.

----
## Requirements
* Java 


----
## Getting started
1. Go to project root directory
2. Go inside web application directory (dcat-viewer-web)
3. Launch the application server ($mvn jetty:run)
4. Run [http://localhost:8080/dcat-viewer/](http://localhost:8080/dcat-viewer/) in your web browser and have fun!


----
## Team

DCAT viewer has been developed within [Fundación CTIC](https://www.fundacionctic.org) by:

* Carlos Tejo-Alonso 			<mailto:carlos.tejo@fundacionctic.org>


----
## Contact

* Carlos Tejo-Alonso 			<mailto:carlos.tejo@fundacionctic.org>
